package PropertyAppsAutomationTesting;

public class MyError extends Exception {

	String errorMsg;
	public MyError(String message)
	{
		this.errorMsg=message;
	}
	public String getError()
	{
		return this.errorMsg;
	}
}
