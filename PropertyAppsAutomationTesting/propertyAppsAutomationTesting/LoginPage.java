package PropertyAppsAutomationTesting;

import java.net.MalformedURLException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;



public class LoginPage extends basePage {
	static boolean email=true;
	static boolean password =true;
	
	static CharSequence input = ("manmeet@propertyapps.com");
	static CharSequence input1 = ("1234");
	
	static WebElement tempObj1;
	static FluentWait<WebDriver> wait;
	public static void LoginPage() throws MalformedURLException {
		
		capabilities();
		//entering password:
		if(email){
			WebDriverWait wait = new WebDriverWait(driver,20);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("txtEmail")));
			
			tempObj1=getElement("txtEmail","ion-input","class");
			tempObj1.sendKeys(input);
			}
		
		//entering password:
		if(password){
			WebDriverWait wait = new WebDriverWait(driver,20);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("txtPassword")));
			tempObj1=getElement("txtPassword","ion-input","class");
			tempObj1.sendKeys(input1);
		}
		
		
		//clicking on Loginbutton:
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("loginBtn")));
		tempObj1=getElement("loginBtn","","id");
		tempObj1.click();
		
//		if (email==false && password == false){
//			System.out.println("enter valid credentails");
//		}
//		else if(email==true && password== true && input1=="----"){
//			System.out.println("enter valid password");
//		}
//		else if (input=="" &&  password == true){
//			System.out.println("Please enter valid email");
//		}
		
		}
	}


