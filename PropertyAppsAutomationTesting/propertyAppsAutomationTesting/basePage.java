package PropertyAppsAutomationTesting;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

import io.appium.java_client.remote.MobileCapabilityType;


public class basePage {
	
	public static AndroidDriver<AndroidElement> driver;

	
	public static void capabilities() throws MalformedURLException {
		// TODO Auto-generated method stub
		 File f = new File("src");
	    File fs = new File(f, "DevApp-release.apk");
	    // File fs =new File (f,"WOsForContractors-release.apk");
	     
	     DesiredCapabilities app = new DesiredCapabilities();
	    app.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
		app.setCapability(MobileCapabilityType.DEVICE_NAME, "Android emulator");
		app.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT,"100");
		//app.setCapability(MobileCapabilityType.DEVICE_NAME, "pixel");
		app.setCapability(MobileCapabilityType.APP, fs.getAbsolutePath());
	   
		driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"),app);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
//		Set <String> s=driver.getContextHandles();
//		for (String handle :s)
//		{
//			System.out.println(handle);
//		}
//		System.out.println(driver.getContext());
//		
//		//WebDriverWait wait = new WebDriverWait(driver,20);
//		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("android.widget.Button"))); 
		
		driver.context("WEBVIEW_com.propertyapps.testing");
	
//		System.out.println(driver.getContext());
		
		
		

	}  
	public static WebElement getElement(String refName,String componentType,String byRef)
	{
		WebElement htmlElement = null;
		if(byRef=="class")
			htmlElement =driver.findElement(By.className(refName));
		
		
		else if(byRef=="id")
			htmlElement =driver.findElement(By.id(refName));
		
		 else if (byRef == "name") 

	            htmlElement = driver.findElement(By.name(refName));
		
			

		switch (componentType) {
			case "ion-input":
			case "ion-textarea":
			
	            htmlElement = htmlElement.findElement(By.className("text-input"));
	            break;
	        case "ion-radio":
	        
	            htmlElement = htmlElement.findElement(By.className("item-cover"));
	            break;
	        
	        case "ion-item":
	        	htmlElement=htmlElement.findElement(By.className("item-inner"));
	        	break;
	        case "button":
	        	htmlElement=htmlElement.findElement(By.className("button"));
	        	break;
	        case "ion-checkbox":
	        	htmlElement=htmlElement.findElement(By.id("permissionToEnter"));
	        	break;
	        
	        case "ion-col":
	        	htmlElement=htmlElement.findElement(By.className("button"));
	        	break;
	        case "input":
	        	htmlElement=htmlElement.findElement(By.className("searchbar-input"));
	        	break;
	        case "ion-datetime":
	        	htmlElement=htmlElement.findElement(By.className("searchbar-input"));
	        	break;
	        case "ion-searchbar":
		        	htmlElement=htmlElement.findElement(By.className("searchbar-input"));
		        	break;
	        	
	        default:
	            break;
		}
		
		return htmlElement;
		
		
		}
	public static List<AndroidElement> returnMatchingElements(String refName,String byRef)
	{
   
        List<AndroidElement> htmlElement = null;
		
        if (byRef == "class") {

            htmlElement =driver.findElements(By.className(refName));
        }
        else if (byRef == "id") {

            htmlElement = driver.findElements(By.id(refName));

        }
        else if (byRef == "tag") {

            htmlElement =driver.findElements(By.tagName(refName)); 

        }
        return htmlElement;
        
        
        

    }
	public static List<WebElement> getRadioButton(String refName,String componentType,String byRef)
	{
		List<WebElement> htmlElement = null;
		WebElement parentElement = null;;
		if(byRef=="id")
			parentElement =driver.findElement(By.id(refName));
		if(byRef=="class")
			parentElement =driver.findElement(By.id(refName));
		
		switch (componentType) {
		case "ion-radio":
            htmlElement = parentElement.findElements(By.className("item-cover"));
            break;
		case "ion-item":
            htmlElement = parentElement.findElements(By.className("item item-block"));
            break;
		case "ion-col":
			 htmlElement = parentElement.findElements(By.className("col"));
			break;
            
        
            
		 default:
	            break;
		}
		
		return htmlElement;
		
		
	
	
	}
	public static void staleElementClick(String refName,String componentType,String byRef )
	{
		WebElement htmlElement = null;
		WebDriverWait wait = new WebDriverWait(driver,15);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(refName)));
		
		htmlElement=getElement(refName,componentType,byRef);
		
		if (byRef=="id")
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(refName)));
		else if(byRef=="name")
		wait.until(ExpectedConditions.presenceOfElementLocated(By.name(refName)));
			
		try
		{
			htmlElement.click();
		}
		catch (Exception e) {
			staleElementClick(refName,componentType,byRef);
		}
	
	}
   }
	




