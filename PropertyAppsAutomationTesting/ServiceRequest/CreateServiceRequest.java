package ServiceRequest;

import java.net.MalformedURLException;
import java.time.Duration;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import PropertyAppsAutomationTesting.MyError;
import PropertyAppsAutomationTesting.basePage;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;

public class CreateServiceRequest extends basePage {

	boolean manualInput = false;
	boolean permissionIsNotAllowed = false;
	static boolean uploadbyCamera = false;
	static boolean uploadPhotobyGallery = true;
	static boolean uploadVideo = false;
	boolean onbehalfofSomeoneElse = true;
	public boolean onbehalfof = false;
	boolean scroll = true;
	boolean scrollUptoUnit = false;
	boolean ableToChooseCategory =false;
	boolean allFieldEmpty=false;
	boolean scrollUptoDescription=true;
	boolean scrollUptocamera=true;
	public boolean scrollUptochooseOption=true;
	public boolean enterScheduleDate=false;
	public boolean clickOnSeheduleDate=true;
	

	int categoryType0 = 1;
	int categoryType1 = 1;
	int issueLocation = 2;
	int priority = 1;
	int assignedToBuildingAssignmentId = 1;
	String unitValue = "1024";
	static String description = "Demo word order created by automated testing";
	String date = "14 12 2018";
	String input = ("//android.view.View[@text='25']");
	String input1 = ("//android.view.View[@text='26']");
	String videoinput = ("//android.widget.TextView[@text='Futuristic_3D_Numbers_Countdown_Seconds__Videvo (1).mov']");
	String videoinput1 = ("//android.widget.TextView[@text='Countdown - 2637.mp4']");
	String setScheduleDateInput=("//android.view.View[@text='30']");
	
	

	static List<AndroidElement> tempList;
	static WebElement tempObj;
	static FluentWait<WebDriver> wait;
	static List<AndroidElement> clickedOption;

	
	
	public void gotoCreateServiceRequestPage() throws MalformedURLException, InterruptedException {
		capabilities();
		WebDriverWait wait = new WebDriverWait(driver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("module1")));
		tempObj = getElement("module1 ", "", "class");
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].scrollIntoView(true);", tempObj);
		tempObj.click();
		tempObj = getElement("addWorkOrderBtn", "", "id");
		tempObj.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if(allFieldEmpty)
		{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("createBehalfOf")));
			onBehalfofSomeoneElse();
			setScheduleDate();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("camera")));
			tempObj=getElement("camera","","name");
			 jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView(true);", tempObj);

			selectCategory();
			
			
			
			scrollUptoDescription();
			
			 submitServiceRequest();
			 unabletoCreateServiceRequest();
			
			
		}
		
		
		
	}

	// Upload Photo by camera:
	public static void uploadPhotoByCamera() throws MalformedURLException, InterruptedException {
		if (uploadbyCamera) {
			 wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("camera")));

			// Clicking on camera button:
			driver.findElementByName("camera").click();

			// Clicking on Upload Photo option:
			tempList = returnMatchingElements("action-sheet-button", "class");
			tempList.get(0).click();

			// Clicking use camera option:
			tempList = returnMatchingElements("action-sheet-button", "class");
			tempList.get(2).click();

			driver.context("NATIVE_APP");

			// on clicking photo by camera:
			driver.findElementById("com.android.camera2:id/shutter_button").click();
			driver.findElementById("com.android.camera2:id/done_button").click();

			// Changing aPP to webView:
			driver.context("WEBVIEW_com.propertyapps.testing");

			// Waiting for delete icon to be visible:
			 wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("trash-icon")));
			tempObj = getElement("trash-icon", "", "class");

			// Scrolling upTo delete icon:
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView(true);", tempObj);
		}
	}

	// Upload photo from gallery
	public static void uploadPhotoFromGallery() throws MalformedURLException, InterruptedException {

		if (uploadPhotobyGallery) {

			 wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("camera")));

			// Clicking on camera button
			driver.findElementByName("camera").click();

			// Click on Upload Photo option
			tempList = returnMatchingElements("action-sheet-button", "class");
			tempList.get(0).click();

			// Click on Select From Gallery option:
			tempList = returnMatchingElements("action-sheet-button", "class");
			tempList.get(3).click();

			driver.context("NATIVE_APP");
			

			// on selecting photo from gallery
			driver.findElementByXPath("//android.widget.TextView[@text='lucas-franco-58883-unsplash.jpg']").click();
			
			// Changing to APP to webView:
			driver.context("WEBVIEW_com.propertyapps.testing");

			// Waiting for delete icon to be visible:
			 wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("trash-icon")));
			tempObj = getElement("trash-icon", "", "class");

			// Scrolling upTo delete icon:
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView(true);", tempObj);
		}
	}

	// Upload Video:
	public void uploadVideoes() {
		if (uploadVideo) {
			 wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("camera")));

			// Clicking on camera button
			driver.findElementByName("camera").click();

			// Click on Upload video option
			tempList = returnMatchingElements("action-sheet-button", "class");
			tempList.get(1).click();
			driver.context("NATIVE_APP");
			

			// Selecting video(more then 10 Seconds):
			driver.findElementByXPath(videoinput).click();

			// Changing to APP to webView:
			driver.context("WEBVIEW_com.propertyapps.testing");
			
			
			
			
			// Click on Ok:
			 wait = new WebDriverWait(driver, 40);
			// Again selecting video :
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("alert-button")));
			tempObj = getElement("alert-button", "", "class");
			tempObj.click();
			driver.findElementByName("camera").click();
			tempList = returnMatchingElements("action-sheet-button", "class");
			tempList.get(1).click();
			driver.context("NATIVE_APP");
			
			// Selecting video(less then 10 Seconds):
			driver.findElementByXPath(videoinput1).click();

			// Changing to APP to webView:
			driver.context("WEBVIEW_com.propertyapps.testing");

			// Waiting for delete icon to be visible:
			 wait = new WebDriverWait(driver, 20);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("trash-icon")));
			tempObj = getElement("trash-icon", "", "class");

			// Scrolling upTo delete icon:
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView(true);", tempObj);

		}

	}

	// Creating on behalf of someone else
	public void onBehalfofSomeoneElse() throws MalformedURLException, InterruptedException {
		if (onbehalfofSomeoneElse) {

			 wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("createBehalfOf")));
			tempObj = getElement("createBehalfOf", "", "id");
			
			tempObj = tempObj.findElement(By.xpath("."));
			tempObj.click();
		}
			// Entering text:
			if (onbehalfof) {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("autoCompleteSearchQuery")));
				tempObj = getElement("autoCompleteSearchQuery", "ion-searchbar", "id");
				tempObj.sendKeys("a");
				driver.pressKeyCode(AndroidKeyCode.BACK);

				tempList = returnMatchingElements("autoCompleteItem[object Object]", "id");
				tempList.get(0).click();
			}
			
			//Scroll upto description:
			if(scrollUptoDescription)
			{
				scrollUptoDescription();
			}
			
			
			

		}
	
	//Setting Schedule date:
	public void setScheduleDate() throws MalformedURLException  {
		if(clickOnSeheduleDate){
			wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("scheduledDate")));
			
			// selecting set schedule date check-box :
			tempObj = getElement("scheduledDate", "", "id");
			tempObj = tempObj.findElement(By.xpath("."));
			tempObj.click();
			
		}
		
		//Entering input :
		if(enterScheduleDate){
			wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("workOrderDataScheduledDate")));
			// selecting set schedule date check-box :
			tempObj = getElement("workOrderDataScheduledDate", "", "id");
			tempObj.click();
			driver.context("NATIVE_APP");
			 
			driver.findElementByXPath(setScheduleDateInput).click();
			driver.findElementById("android:id/button1").click();
			driver.context("WEBVIEW_com.propertyapps.testing");
			
			
		}
		
	}
	
	
	

	// Selecting Category Option:
	public void selectCategory() throws MalformedURLException, InterruptedException {
		 wait = new WebDriverWait(driver, 40);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("categoryType0")));
		 // Clicking on Category Option:
		 driver.findElementById("categoryType0").click();
		 // Select Category Option:
		 tempList = returnMatchingElements("item-radio", "class");
		 if (this.manualInput)
			 this.categoryType0 = inputForSelectBox(tempList);
		 else
			 tempList.get(this.categoryType0).click();
		 
		 //Scroll upto choose option when all above fields are filled:
       if(scrollUptocamera){
  	   wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("camera")));
			
			tempObj = getElement("camera", "", "name");
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			 jse.executeScript("arguments[0].scrollIntoView(true);", tempObj);
  	   
       	   
          }
		
			
         if (ableToChooseCategory){
        	// Clicking on Choose Option
   			WebDriverWait wait = new WebDriverWait(driver, 30);
   			staleElementClick("categoryType1", "", "id");
   			// Selecting Choose Option
   			tempList = returnMatchingElements("item-radio", "class");
   			if (this.manualInput)
   				this.categoryType1 = inputForSelectBox(tempList);
   			else
   				tempList.get(this.categoryType1).click();
           }
           


        	   
			
			// Click on IssueLocationId option:
			 wait = new WebDriverWait(driver, 30);
			staleElementClick("issueLocationId", "", "id");

			// Selecting Option:
			tempList = returnMatchingElements("item-radio", "class");
			String output = null;
			if (this.manualInput)
				this.issueLocation = inputForSelectBox(tempList);
			else {
				output = tempList.get(this.issueLocation).getText();
				tempList.get(this.issueLocation).click();
			}
			if (output.equals("In the unit")) {

				if (scrollUptoUnit) {
					tempObj = getElement("categoryType1", "", "id");
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("categoryType1")));
					JavascriptExecutor jse11 = (JavascriptExecutor) driver;
					jse11.executeScript("arguments[0].scrollIntoView(true);", tempObj);
				}
				tempObj = getElement("unit", "", "id");
				tempObj.click();
				// on selecting unit:
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("selectBoxSearchBox")));
				tempObj = getElement("selectBoxSearchBox", "input", "id");
				tempObj.sendKeys("1");
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("button-full-md")));
				tempObj = getElement("button-full-md", "", "class");
				tempObj.click();

				// on selecting floor:
				driver.findElementById("floor").click();
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("selectBoxSearchBox")));
				tempObj = getElement("selectBoxSearchBox", "input", "id");
				tempObj.sendKeys("1");
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("button-full-md")));
				tempObj = getElement("button-full-md", "", "class");
				tempObj.click();

			}

			else if (output.equals("In an interior common area")) {
				// on selecting floor:
				driver.findElementById("floor").click();
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("searchbar-input-container")));
				tempObj = getElement("searchbar-input-container", "input", "class");
				tempObj.sendKeys("1");
			}

			else {
				tempObj = getElement("where", "ion-input", "id");
				tempObj.sendKeys(description);
				driver.pressKeyCode(AndroidKeyCode.BACK);
			}
		}

		
			
		
		
	// When option "Manager has permission to fix the issue when no one is
	// there" is disable
	public void permissiontoFixIssue() throws MalformedURLException, InterruptedException {
		if (this.permissionIsNotAllowed) {
			WebDriverWait wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("permissionToEnter")));
			tempObj = getElement("permissionToEnter", "", "id");
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView(true);", tempObj);
			tempObj = tempObj.findElement(By.xpath("."));
			tempObj.click();

			// Option1: mM/dD/yyyY:
			tempObj = getElement("allowedDateToEnter1", "", "id");
			tempObj.click();
			driver.context("NATIVE_APP");

			driver.findElementByXPath(input).clear();
			driver.findElementById("android:id/button1").click();
			driver.context("WEBVIEW_com.propertyapps.testing");

			// Option1:time From:
			tempObj = getElement("allowedTimeToEnter1From", "", "id");
			tempObj.click();
			// time:
			tempList = returnMatchingElements("picker-opt", "class");
			tempList.get(2).click();

			// click on done:
			tempList = returnMatchingElements("picker-toolbar-button", "class");
			tempList.get(1).click();

			// Option1:time To:
			tempObj = getElement("allowedTimeToEnter1To", "", "id");
			tempObj.click();
			// time:
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			tempList = returnMatchingElements("picker-opt", "class");
			tempList.get(2).click();
			// click on done:
			tempList = returnMatchingElements("picker-toolbar-button", "class");
			tempList.get(1).click();

			// Option2: mM/dD/yyyY:
			tempObj = getElement("allowedDateToEnter2", "", "id");
			tempObj.click();
			driver.context("NATIVE_APP");

			driver.findElementByXPath(input1).clear();
			driver.findElementById("android:id/button1").click();

			driver.context("WEBVIEW_com.propertyapps.testing");

			// Option2:time From:
			tempObj = getElement("allowedTimeToEnter2From", "", "id");
			tempObj.click();

			// time:
			tempList = returnMatchingElements("picker-opt", "class");
			tempList.get(3).click();

			// click on done:
			tempList = returnMatchingElements("picker-toolbar-button", "class");
			tempList.get(1).click();

			// Option1:time To:
			tempObj = getElement("allowedTimeToEnter2To", "", "id");
			 jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView(true);", tempObj);
			tempObj.click();

			// time:
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			tempList = returnMatchingElements("picker-opt", "class");
			tempList.get(3).click();

			// click on done:
			tempList = returnMatchingElements("picker-toolbar-button", "class");
			tempList.get(1).click();

			// Scrolling upTo Priority option:
			if (scroll) {
				 wait = new WebDriverWait(driver, 40);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("priority")));
				tempObj = getElement("priority", "", "id");
				 jse = (JavascriptExecutor) driver;
				jse.executeScript("arguments[0].scrollIntoView(true);", tempObj);

			}

		}
	}

	public void priorityAndAssignOption() throws MalformedURLException, InterruptedException {
		WebDriverWait wait1111 = new WebDriverWait(driver, 40);
		wait1111.until(ExpectedConditions.visibilityOfElementLocated(By.id("priority")));
		// Clicking on Priority Option":
		driver.findElementById("priority").click();
		// Selecting Priority():
		tempList = returnMatchingElements("item-radio", "class");
		if (this.manualInput)
			this.priority = inputForSelectBox(tempList);
		else
			tempList.get(this.priority).click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

		// Clicking on Assign Option:
		driver.findElementById("assignedToBuildingAssignmentId").click();
		// Selecting the person to whom work is to be assign:
		tempList = returnMatchingElements("item-radio", "class");
		if (this.manualInput)
			this.assignedToBuildingAssignmentId = inputForSelectBox(tempList);
		else
			tempList.get(this.assignedToBuildingAssignmentId).click();
	}

	public static void addDescription() throws MalformedURLException, InterruptedException {
		// Entering descipTion
		WebDriverWait wait = new WebDriverWait(driver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("workOrderDescription")));
		tempObj = getElement("workOrderDescription", "ion-input", "id");
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].scrollIntoView(true);", tempObj);
		
	
		tempObj.sendKeys(description);
		driver.pressKeyCode(AndroidKeyCode.BACK);
	}
		
		public static void submitServiceRequest() throws MalformedURLException, InterruptedException 
		{
			// Clicking on Submit button:
			WebDriverWait wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("submitBtn")));
			driver.findElementById("submitBtn").click();
		}
		
		public static void ableTocreatedServiceRequest() throws MalformedURLException, InterruptedException
		{ 
			WebDriverWait wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("alert-title")));
			tempObj = getElement("alert-title", "", "class");
			System.out.println("When able to create service request  succesfully pop-up message appear  :"+ tempObj.getText());
			 wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("alert-button")));
			
			tempObj = getElement("alert-button", "", "class");
			tempObj.click();
		}
		
		public static void unabletoCreateServiceRequest() throws MalformedURLException, InterruptedException
		{
			WebDriverWait wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("alert-subhdr-0")));
			tempObj = getElement("alert-subhdr-0", "", "id");
			System.out.println("When Schedule Date,Category and Description fields are empty : " +tempObj.getText());
			wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("alert-button")));
				
			tempObj = getElement("alert-button", "", "class");
			tempObj.click();
			
			
				
			}
			
		
	
		public void onBehalfOfSomeonePopupMessage() throws MalformedURLException, InterruptedException
		{
			WebDriverWait wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("alert-sub-title")));
			tempObj = getElement("alert-sub-title", "", "class");
			System.out.println("When i'm created on  behalf of someone else field is left empty : "+tempObj.getText());
			wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("alert-button")));
				
			tempObj = getElement("alert-button", "", "class");
			tempObj.click();
			
			wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("text-md-danger")));
			tempObj = getElement("text-md-danger", "", "class");
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView(true);", tempObj);
			
			
		}
		public static void scrollUptoDescription() throws MalformedURLException, InterruptedException
		{
			wait = new WebDriverWait(driver, 40);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("workOrderDescription")));
			
			tempObj = getElement("workOrderDescription", "ion-input", "id");
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			 jse.executeScript("arguments[0].scrollIntoView(true);", tempObj);
		}
		
		public static void scrollUptoCamera() throws MalformedURLException, InterruptedException
		{
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("camera")));
				
				tempObj = getElement("camera", "", "name");
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				 jse.executeScript("arguments[0].scrollIntoView(true);", tempObj);
		}
		
		
		
		

	private static int inputForSelectBox(List<AndroidElement> list) {
		String clickedOption;
		System.out.println("---------------Choose a value---------------");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(i + " " + list.get(i).getText());
		}

		// to get input from keyboard
		Scanner scanner = new Scanner(System.in);
		int input = scanner.nextInt();// 0,1,2
		clickedOption = list.get(input).getText();
		list.get(input).click();
		return input;
	}

	// Function: Create serviceRequest as Manager:
	public void createServiceRequestAsManager() throws MalformedURLException, InterruptedException, MyError {
		gotoCreateServiceRequestPage();
		try
		{
			uploadPhotoFromGallery();
		}
		catch(Exception e)
		{
			throw new MyError("Error:Problem in uploading photo from gallery "); 
		}
		
		
		try{
			uploadPhotoByCamera();
		
		}
		catch(Exception e)
		{
			throw new MyError("Error: Problem in uploading  Photo by Camera");
		}
		
		
		try{
			 uploadVideoes();
			 
		 }
		catch(Exception e)
		{
			throw new MyError("Error: Problem in uploading videoe");
		}
		
		try{
			
			onBehalfofSomeoneElse();
		 }
		catch(Exception e)
		{
			throw new MyError("Error: Problem in Selecting Check-box (I'm creating on behalf of someone else");
		}
		
        try{
			
        	setScheduleDate();
		 }
		catch(Exception e)
		{
			throw new MyError("Error: Problem in Selecting Check-box (Set Schedule Date");
		}
		
		
		
		
		try{
			
			selectCategory();
		}
		catch(Exception e)
		{
			throw new MyError("Error: Problem in Selecting Category");
		}
		
		
		try{
			permissiontoFixIssue();
		}
		catch(Exception e)
		{
			throw new MyError("Error: Problem in check-box(Managment has the permission to fix the issue when no one is there)");
		}
		
		
		
		try{
			priorityAndAssignOption();
		}
		catch(Exception e)
		{
			throw new MyError("Error: Problem in selecting Priority or Assign option");
		}
		
		try{
			addDescription();
		}
		catch(Exception e)
		{
			throw new MyError("Error: Problem on entering text in Description option");
		}
		
		
		try{
			submitServiceRequest();
		}
		catch(Exception e)
		{
			throw new MyError("Error: Problem on clicking the Submit Button");
		}
		
		try{
			ableTocreatedServiceRequest();
		}
		catch(Exception e)
		{
			throw new MyError("Error: Unable to create service request");
		}
		
		
		 
		
		
		
		
		
		
		

	}

	// Function:Create serviceRequest as Tenant:
	public void createServiceRequestAsTenant() throws MalformedURLException, InterruptedException {
		gotoCreateServiceRequestPage();
		uploadPhotoByCamera();
		uploadPhotoFromGallery();
		selectCategory();
		permissiontoFixIssue();
		submitServiceRequest();
	}

}
