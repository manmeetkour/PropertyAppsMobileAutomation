package ServiceRequest;

import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import PropertyAppsAutomationTesting.basePage;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;

public class UpdateServiceRequestPage extends basePage {
	static WebElement tempObj;
	static List<AndroidElement> tempList ;
	static FluentWait<WebDriver> wait;
	
	 static String workOrderinput=("workOrder0");
	 static String addNoteInput=("adding a note ");
	 static String materialInput=("1. Cement \n 2. wires \n ");
	 static String costInput=("120 ");
	 static String workDoneNoteInput=("This note is visible to everyone ");
	 static String noteHiddenFromTenantsInput=("Only manager can see this Note");
	 static int completeYesNoInput=0;
	 static int priorityInput=0;
	 static String endDateInput=("//android.view.View[@text='23']");
	 static String endDateInput1=("//android.view.View[@text='30']");
	 static String endTimeInput=("//android.widget.Button[@text='2']") ;
	 static String endTimeInput1=("//android.widget.Button[@text='3']") ;
	 
	public static void gotoUpdateServiceRequest() throws MalformedURLException
	{
		// TODO Auto-generated method stub
		capabilities();
		
		WebDriverWait wait = new WebDriverWait(driver,40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("module1")));
		
		WebElement tempObj1;
        tempObj1=getElement("module1 ","","class");
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].scrollIntoView(true);", tempObj1);
		tempObj1.click();
		WebDriverWait wait1 = new WebDriverWait(driver,40);
		wait1.until(ExpectedConditions.visibilityOfElementLocated(By.id("workOrder0")));
		tempObj1 = getElement("workOrder0","ion-text","id");
		tempObj1.click();
	}
	
	public static void clickUpdateBtn() throws MalformedURLException
	{
		WebDriverWait wait1111 = new WebDriverWait(driver,40);
		wait1111.until(ExpectedConditions.visibilityOfElementLocated(By.id("updateBtn")));
		tempObj=getElement("updateBtn","","id");
		tempObj.click();
	}
	
	public static void editServiceRequest() throws MalformedURLException, InterruptedException
	{
		//Click on Update button:
		clickUpdateBtn();
		
		//Click on Edit Option:
		WebDriverWait wait = new WebDriverWait(driver,40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("action-sheet-button")));
		tempList=returnMatchingElements("action-sheet-button","class");
		tempList.get(0).click();
		 wait = new WebDriverWait(driver,60);
        
		 //Scroll upto description:
		CreateServiceRequest.scrollUptoDescription();
		
		//Deleting text from description field:
		tempObj = getElement("workOrderDescription", "ion-input", "id");
		tempObj.clear();
		tempObj.sendKeys("1");
		tempObj.sendKeys(Keys.BACK_SPACE);
		tempObj.sendKeys(Keys.BACK_SPACE);
		
		driver.pressKeyCode(AndroidKeyCode.BACK);
		CreateServiceRequest.submitServiceRequest();
		CreateServiceRequest.unabletoCreateServiceRequest();
		
		
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("workOrderDescription")));
		tempObj = getElement("workOrderDescription", "ion-input", "id");
		tempObj.sendKeys("Demo test");
		driver.pressKeyCode(AndroidKeyCode.BACK);
		
		CreateServiceRequest.submitServiceRequest();
		CreateServiceRequest.ableTocreatedServiceRequest();
		
		
		
	}
	
	public static void addNotetoServiceRequest() throws MalformedURLException
	{
		//Clicking on Update Button:
		clickUpdateBtn();
		
		//Click on Add Note Option:
		List<AndroidElement> tempList = returnMatchingElements("action-sheet-button","class");
		tempList.get(1).click();
		
		//On leaving the Add Note fild  empty:
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		tempObj=getElement("inputValue","ion-textarea","name");
		tempObj.sendKeys("");
		driver.findElementByXPath("/html/body/ion-app/ion-modal/div/ng-component/ion-content/div[2]/form/button").click();
		WebDriverWait wait = new WebDriverWait(driver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("alert-subhdr-0")));
		tempObj = getElement("alert-subhdr-0", "", "id");
		System.out.println("When Add Note field are empty : " +tempObj.getText());
		wait = new WebDriverWait(driver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("alert-button")));
		tempObj = getElement("alert-button", "", "class");
		tempObj.click();
		
		//Entering Service Request Note:
		tempObj=getElement("inputValue","ion-textarea","name");
		tempObj.sendKeys(addNoteInput);
		driver.findElementByXPath("/html/body/ion-app/ion-modal/div/ng-component/ion-content/div[2]/form/button").click();
		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
		driver.pressKeyCode(AndroidKeyCode.BACK);
		//driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		
		
	}
	
	public static void addWorkUpdatetoServiceRequest() throws MalformedURLException
	{
		//Click on update Button:
		clickUpdateBtn();
		
		
		//Click on Add work Update Option:
		tempList=returnMatchingElements("action-sheet-button","class");
		tempList.get(2).click();
		
		//Clicking on submit button without changing anything:
		 wait = new WebDriverWait(driver, 40);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("submitJobBtn")));
		tempObj=getElement("submitJobBtn","","id");
        JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].scrollIntoView(true);", tempObj);
		tempObj.click();
		WebDriverWait wait = new WebDriverWait(driver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("alert-subhdr-0")));
		tempObj = getElement("alert-subhdr-0", "", "id");
		System.out.println("Pop-up Message:When no changes is made in Add Work Update Page  : " +tempObj.getText());
		wait = new WebDriverWait(driver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("alert-button")));
		tempObj = getElement("alert-button", "", "class");
		tempObj.click();
		
		// scroll up to "Back to service request button":
		wait = new WebDriverWait(driver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("bankToServiceReqBtn")));
		tempObj = getElement("bankToServiceReqBtn", "", "id");
		
		
		//Entering text in Material used textArea:
		tempObj=getElement("materialsUsed","ion-textarea","name");
		tempObj.sendKeys(materialInput);
		
		//Amount of money spend:
		tempObj=getElement("amountSpent","ion-input","id");
		tempObj.sendKeys(costInput);
		driver.pressKeyCode(AndroidKeyCode.BACK);
		
		//Entering text in WorkDone field which should be visBile to everyone
		tempObj=getElement("workDoneNote","ion-textarea","id");
		tempObj.sendKeys(workDoneNoteInput);
		driver.pressKeyCode(AndroidKeyCode.BACK);
		
		//Entering  HIDDEN NOTE(visBile only to managers):
		tempObj=getElement("noteHiddenFromTenants","ion-textarea","id");
		tempObj.sendKeys(noteHiddenFromTenantsInput);
		driver.pressKeyCode(AndroidKeyCode.BACK);
		
		//Clicking on Completed Option under STATUS:
		tempObj=getElement("completeYesNo","","id");
		tempObj.click();
		
		//On selecting "NO":
		tempList= returnMatchingElements("item-radio","class");
		tempList.get(completeYesNoInput).click();
		
		//Auto-set end time on submit:
		
		 wait = new WebDriverWait(driver, 40);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("autoSetEndTime")));
		 tempObj = getElement("autoSetEndTime", "", "id");
			
		 tempObj = tempObj.findElement(By.xpath("."));
		 tempObj.click();
		 
		 //Selecting end date:
		 tempObj = getElement("endDate", "", "id");
		 tempObj.click();
		 driver.context("NATIVE_APP");
		 
		//if end date is less then present date:
		driver.findElementByXPath(endDateInput).click();
		driver.findElementById("android:id/button1").click();
		driver.context("WEBVIEW_com.propertyapps.testing");
		System.out.println("Error Message:"+ driver.findElementByClassName("invalid-field").getText());
		
		//If end date is equal to or greater then present date:
		 tempObj = getElement("endDate", "", "id");
		 tempObj.click();
		 driver.context("NATIVE_APP");
		 
		
		driver.findElementByXPath(endDateInput1).click();
		driver.findElementById("android:id/button1").click();
		driver.context("WEBVIEW_com.propertyapps.testing");
		
		//Scroll upto end time:
		tempObj=getElement("endTime", "", "id");
		jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].scrollIntoView(true);", tempObj);
		
		
		//If end time is  greater then present date:
		 tempObj = getElement("endTime", "", "id");
		 tempObj.click();
		 tempList = returnMatchingElements("picker-opt", "class");
		 tempList.get(2).click();

		// click on done:
		tempList = returnMatchingElements("picker-toolbar-button", "class");
		tempList.get(1).click();
		
		System.out.println("Bug:Red Color error message should appear");
		
		//Click on Submit Button
		tempObj=getElement("submitJobBtn","","id");
        jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].scrollIntoView(true);", tempObj);
		tempObj.click();
		
		// Click "Ok" on  work Update Added Popup:
		WebDriverWait wait1 = new WebDriverWait(driver,40);
		wait1.until(ExpectedConditions.visibilityOfElementLocated(By.className("alert-button")));
		tempObj=getElement("alert-button","","class");
		tempObj.click();
		
		//Pop-Up message :"Do you want to add a photo to show how it looks like"
		wait = new WebDriverWait(driver, 40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("alert-title")));
		tempObj = getElement("alert-title", "", "class");
		System.out.println("Pop-Up message  : " +tempObj.getText());

		WebDriverWait wait11 = new WebDriverWait(driver,40);
		wait11.until(ExpectedConditions.visibilityOfElementLocated(By.className("alert-button")));
		tempList= returnMatchingElements("alert-button","class");
		tempList.get(0).click();
		driver.pressKeyCode(AndroidKeyCode.BACK);
		driver.pressKeyCode(AndroidKeyCode.BACK);
	}
	
	public static void changePriority() throws MalformedURLException
	{
		//Clicking on update Button:
		clickUpdateBtn();
		
		tempList=returnMatchingElements("action-sheet-button","class");
		tempList.get(3).click();
		
		//On Selecting Low Priority:
		tempList= returnMatchingElements("item-radio","class");
		tempList.get(priorityInput).click();
		
		//Click "YES" or "NO" on PoPup (Change the priority to low/medium/high): 
		WebDriverWait wait2 = new WebDriverWait(driver,40);
		wait2.until(ExpectedConditions.visibilityOfElementLocated(By.className("alert-button")));
		tempList= returnMatchingElements("alert-button","class");
		tempList.get(1).click();
	}
	
	public static void reassignServiceRequest() throws MalformedURLException
	{
		//Click on update button:
		clickUpdateBtn();
		//reassign option:
		tempList=returnMatchingElements("action-sheet-button","class");
		tempList.get(4).click();
		
		tempList= returnMatchingElements("item-radio","class");
		tempList.get(0).click();
		//Clicking on "YES" or "NO" on PoPup(Change assign to ):
		WebDriverWait wait02 = new WebDriverWait(driver,40);
		wait02.until(ExpectedConditions.visibilityOfElementLocated(By.className("alert-button")));
		tempList= returnMatchingElements("alert-button","class");
		tempList.get(1).click();
	}
	
	public static void markCompletedtoServiceRequest() throws MalformedURLException
	{
		//click on update button:
		clickUpdateBtn();
		//Mark completed:
		tempList=returnMatchingElements("action-sheet-button","class");
		tempList.get(5).click();
		//Clicking on "YES" or "NO" on PoPup(Change assign to ):
		WebDriverWait wait021 = new WebDriverWait(driver,40);
		wait021.until(ExpectedConditions.visibilityOfElementLocated(By.className("alert-button")));
		tempList= returnMatchingElements("alert-button","class");
		tempList.get(0).click();
	}
	
	public static void scrollUptoDescription() throws MalformedURLException
	{
        
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("workOrderDescription")));
		
		tempObj = getElement("workOrderDescription", "ion-input", "id");
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		 jse.executeScript("arguments[0].scrollIntoView(true);", tempObj);
	}
	
	public  void updatingServiceRequestAsManager() throws MalformedURLException, InterruptedException
	{
		gotoUpdateServiceRequest();
		
		//editServiceRequest();
		//addNotetoServiceRequest();
		addWorkUpdatetoServiceRequest();
		changePriority();
		reassignServiceRequest();
		markCompletedtoServiceRequest();
	}
	
	public static void updatingServiceRequestAsTenant() throws MalformedURLException, InterruptedException
	{
		gotoUpdateServiceRequest();
		clickUpdateBtn();
		editServiceRequest();
		
	}


}


		
	
	
		
		
		
	
		
	
		
	
       	
		
		

			
			
			
			
			
		
			
		
