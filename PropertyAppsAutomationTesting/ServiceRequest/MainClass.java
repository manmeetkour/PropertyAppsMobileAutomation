package ServiceRequest;

import java.net.MalformedURLException;
import java.util.Scanner;

import PropertyAppsAutomationTesting.LoginPage;
import PropertyAppsAutomationTesting.MyError;

public class MainClass {

	public static void main(String[] args) throws MalformedURLException, InterruptedException {
		
		while(true){
			System.out.println("What would you like to test? "
					+ "\n 1: Login Page"
					+ "\n 2: Create Service Request As Manager With Default values"
					+ "\n 3: Create Service Request As Tenant With Default Value"
					+ "\n 4: Update Service Request As Manager with Default Value"
					+ "\n 5: Update Service Request As Tenant with Default Value"
					+ "\n 6: Search Service Request Page "
					+ "\n 7: Filtering And Sorting Page"
					+ "\n 8: Log Page"
					+ "\n 9: UploadPage"
					+ "\n 10: Exit");
			
			
			Scanner scanner = new Scanner(System.in);
			int input = scanner.nextInt(); 
			 
			 switch(input) {
			 
			 case 1:
				 LoginPage login =new LoginPage();
//				 login.email=false;
//				 login.password=false;
//				 login.LoginPage();
//				 
//				 login.email=true;
//				 login.password=true;
//				 login.input1=("----");
//				 login.LoginPage();
				 
				
				
				
//				 login.input=("manmeetpropertyappscom");
//				 login.input1=("1234");
//				 login.LoginPage();
//				 
//				login.input=("manmeet@propertyapps.com");
//				login.password=true;
//				 login.LoginPage();
				 break;
				 
				 
			 case 2:
				 
				 CreateServiceRequest obj = new CreateServiceRequest();
				 try{
					 obj.allFieldEmpty=true;
					 obj.ableToChooseCategory=false;
					 obj.scrollUptocamera=true;
					 obj.scrollUptoDescription=false;
					 obj.gotoCreateServiceRequestPage();
					 
					 obj.scrollUptoCamera();
					 obj.clickOnSeheduleDate=false;
					 obj.enterScheduleDate=true;
					 obj.setScheduleDate();
					 obj.ableToChooseCategory=true;
					 obj.scrollUptochooseOption=true;
					 obj.selectCategory();
					 obj.addDescription();
					 obj.submitServiceRequest();
					 obj.onBehalfOfSomeonePopupMessage();
					 
					 obj.scrollUptoCamera();
					 obj.onbehalfofSomeoneElse=false;
					 obj.onbehalfof=true;
					 obj.scrollUptoDescription=true;
					 obj.onBehalfofSomeoneElse();
					 obj.submitServiceRequest();
					 obj.ableTocreatedServiceRequest();
					 System.out.println("All the Validation case of Service Request: PASSED");
				 }
				 catch(Exception e ){
					 System.out.println("Problem in checking the valid case");
				 }
				 
				 
				 
				 obj.onbehalfofSomeoneElse=true;
				 obj.scrollUptoDescription=false;
				 obj.permissionIsNotAllowed=true;
				 obj.allFieldEmpty=false;
				 
				 try{
					 obj.createServiceRequestAsManager();
					 System.out.println("First test of Service Request: PASSED");
				 }
				 catch(MyError e){
					 System.out.println(e.getError());
					 }
				
				
			     obj.uploadPhotobyGallery=false;
			     obj.uploadVideo=true;
			     obj.allFieldEmpty=false;
			     obj.ableToChooseCategory=true;
			     obj.scrollUptocamera=false;
			     
				 obj.onbehalfofSomeoneElse =false;
				 obj.scrollUptoUnit=false;
				 obj.permissionIsNotAllowed=false;
				 try{
					 obj.createServiceRequestAsManager();
					 System.out.println("Second test case of Service Request: PASSED");
					 
				 }
				 catch(MyError e){
					 System.out.println(e.getError());
					 System.out.println("------------------------------------------------------------------------------------");
					 
				 }
				 
				
				
				 obj.uploadVideo=false;
				 obj.uploadbyCamera=true;
				 obj.permissionIsNotAllowed=false;
				 obj.onbehalfof=false;
				 try{
					 obj.createServiceRequestAsManager();
					 System.out.println("Third test case of Service Request: PASSED");
					 }
				 catch(MyError e){
					 System.out.println(e.getError());
					 System.out.println("------------------------------------------------------------------------------------");
				 }
				 
				 
				
				
				 
				 	
				 
				 break;
				 
             case 3:
            	 
            	 CreateServiceRequest obj1 = new CreateServiceRequest();
				 obj1.manualInput=false;
				 obj1.scroll=false;
				 try{
					 obj1.createServiceRequestAsManager();
					 System.out.println("First test case of Service Request: PASSED");
					 }
				 catch(MyError e){
					 System.out.println(e.getError());
				 }
				 
				 obj1.uploadPhotobyGallery=false;
				 obj1.permissionIsNotAllowed=false;
				 obj1.scroll=false;
				 try{
					 obj1.createServiceRequestAsManager();
					 System.out.println("Second test case of Service Request: PASSED");
					 }
				 catch(MyError e){
					 System.out.println(e.getError());
				 }
				 
				 obj1.uploadPhotobyGallery=true;
				 obj1.permissionIsNotAllowed=true;
				 obj1.input=("//android.view.View[@text='19']");
				 obj1.input1 = ("//android.view.View[@text='19']");
				 obj1.scroll=false;
				 try{
					 obj1.createServiceRequestAsManager();
					 System.out.println("Third test case of Service Request: PASSED");
					 }
				 catch(MyError e){
					 System.out.println(e.getError());
				 }
                 break;
                 
			 case 4:
				 UpdateServiceRequestPage update = new UpdateServiceRequestPage();
				 update.updatingServiceRequestAsManager();
				break;
			 case 5:
				 UpdateServiceRequestPage update1 = new UpdateServiceRequestPage();
				 update1.updatingServiceRequestAsTenant();
				 
				break;
			 case 6:
				 SearchServiceRequest search=new SearchServiceRequest();
				 search.searchServiceRequest(); 
				 
				 break;
			 case 7:
				 FilterAndSort filter = new  FilterAndSort();
				 try{
					 filter.filteringAndSorting();
					 System.out.println("First test case of Showing Filter : PASSED");
					 System.out.println("------------------------------------------------------------------------------------");
					 }
				 catch(MyError e){
					 System.out.println(e.getError());
				 }
				 break;
			 case 8:
				 LogPage log =new LogPage();
				 log.logServiceRequest();
				 
			 case 9:
				 System.exit(0);
				 break;
			 default:
				 System.out.println("Wrong choice entered. Please enter again");
			 }
		}

	}

}
