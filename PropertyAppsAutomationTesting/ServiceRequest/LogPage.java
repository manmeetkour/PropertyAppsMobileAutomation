package ServiceRequest;

import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import PropertyAppsAutomationTesting.basePage;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;

public class LogPage extends basePage {
	 static String workOrderInput= ("workOrder0");
	 static WebElement tempObj1 ;
	 static List<AndroidElement> tempList;

	public static void gotoLogPage() throws MalformedURLException {
		// TODO Auto-generated method stub
		capabilities();
		WebDriverWait wait = new WebDriverWait(driver,40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("module1")));
		
		WebElement tempObj1;
        tempObj1=getElement("module1 ","","class");
        
		
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].scrollIntoView(true);", tempObj1);
		tempObj1.click();
		
		WebDriverWait wait111 = new WebDriverWait(driver,40);
		wait111.until(ExpectedConditions.visibilityOfElementLocated(By.id(workOrderInput)));
		tempObj1=getElement(workOrderInput,"ion-text","id");
		tempObj1.click();
	}
	
	
	
	public static void clickLogButton() throws MalformedURLException {
	
		
		//Click on LogBtn:
		WebDriverWait wait1111 = new WebDriverWait(driver,40);
		wait1111.until(ExpectedConditions.visibilityOfElementLocated(By.id("logBtn")));
		WebElement tempObj1 = getElement("logBtn","","id");
		tempObj1.click();
	}
	
	public static void workHistoryLog() throws MalformedURLException {
		
		//Click on Log button:
		clickLogButton();
		//Click on Work History: 
		WebDriverWait wait1 = new WebDriverWait(driver,40);
		wait1.until(ExpectedConditions.visibilityOfElementLocated(By.className("action-sheet-button")));
		tempList=returnMatchingElements("action-sheet-button","class");
		tempList.get(0).click();
		driver.pressKeyCode(AndroidKeyCode.BACK);
	}
	
	public static void phototesOrVideosLog() throws MalformedURLException {	
		//Click on Log button:
		clickLogButton();
		
		//Click on Photo/Videos:
		tempList=returnMatchingElements("action-sheet-button","class");
		tempList.get(1).click();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.pressKeyCode(AndroidKeyCode.BACK);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	}
	public static void ReceiptsLog() throws MalformedURLException {
		//Click on Log button:
		clickLogButton();
		
		//Click on Receipts:
		tempList=returnMatchingElements("action-sheet-button","class");
		tempList.get(2).click();
		driver.pressKeyCode(AndroidKeyCode.BACK);
	}
	
	public static void logServiceRequest() throws MalformedURLException {
		gotoLogPage();
		workHistoryLog();
		phototesOrVideosLog();
		ReceiptsLog();
	
		
  }
}


