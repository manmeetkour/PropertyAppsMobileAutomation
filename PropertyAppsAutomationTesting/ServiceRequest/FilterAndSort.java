package ServiceRequest;

import java.io.DataInput;
import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import PropertyAppsAutomationTesting.MyError;
import PropertyAppsAutomationTesting.basePage;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;

public class FilterAndSort extends basePage {
	
	static List<WebElement> tempList;
	static WebElement tempObj1;
	static FluentWait<WebDriver> wait;
	
	static int sortingInput=2;
	static int inputStatus=2;
	static int inputPriority=2;
	static int inputAssignTo=0;
	static String costGreaterThanInput=("100");
	static String costLessThanInput=("100");
    static String dateCreatedAfterInput=("//android.view.View[@text='8']");
    static String dateCreatedBeforeInput=("//android.view.View[@text='9']");
    static String dateUpdatedAfter=("//android.view.View[@text='8']");
    static String dateUpdatedBefore=("//android.view.View[@text='8']");
    
    static boolean SortOption=true;
    static boolean createdOption=true;
    static boolean updatedBy=true;
    static boolean statusOption=true;
    static boolean priorityOption =true;
    static boolean costOption=true;
    static boolean assignedToOption=true;

	public static void gotoServiceRequest() throws MalformedURLException {
		// TODO Auto-generated method stub
		capabilities();
		WebDriverWait wait = new WebDriverWait(driver,40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("module1")));
		
		
        tempObj1=getElement("module1 ","","class");
		
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].scrollIntoView(true);", tempObj1);
		
		tempObj1.click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.findElementById("openFiltersBtn").click();
	}
	
	public static void selectingSortingOption() throws MalformedURLException {
		if(SortOption){
			WebDriverWait wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("sortBy")));
			
			tempList=getRadioButton("sortBy","ion-radio","id");
			
			tempList.get(sortingInput).click();
			
		}
		
		
	}
	
	public static void filterByCreatedDate() throws MalformedURLException {
		if(createdOption){
			WebDriverWait wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("createdSinceFilterValue")));
			//Clicking on After input box:
			driver.findElementById("createdSinceFilterValue").click();
			driver.context("NATIVE_APP");
			
			//On selecting previous month:
			driver.findElementById("android:id/prev").click();
			driver.findElementById("android:id/prev").click();
			
			//On selecting date:
			driver.findElementByXPath(dateCreatedAfterInput).click();
			//On clicking SET option:
			driver.findElementById("android:id/button1").click();
			driver.context("WEBVIEW_com.propertyapps.testing");
			
			//Clicking on Before input box:
			WebDriverWait wait1 = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("createdBeforeFilterValue")));
			//Clicking on After input box:
			driver.findElementById("createdBeforeFilterValue").click();
			driver.context("NATIVE_APP");
			
			//On selecting previous month:
			driver.findElementById("android:id/prev").click();
			driver.findElementById("android:id/prev").click();
			
			//On selecting date:
			driver.findElementByXPath(dateCreatedBeforeInput).click();
			//On clicking SET option:
			driver.findElementById("android:id/button1").click();
			driver.context("WEBVIEW_com.propertyapps.testing");
			
		}
		
	}
		
	public static void filterByUpdatedDate() throws MalformedURLException
	{ 
		if(updatedBy){
			WebDriverWait wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("updatedSinceFilterValue")));
			//Clicking on After input box:
			driver.findElementById("updatedSinceFilterValue").click();
			driver.context("NATIVE_APP");
			
			//On selecting previous month:
			driver.findElementById("android:id/prev").click();
			driver.findElementById("android:id/prev").click();
			
			//On selecting date:
			driver.findElementByXPath(dateUpdatedAfter).click();
			//On clicking SET option:
			driver.findElementById("android:id/button1").click();
			driver.context("WEBVIEW_com.propertyapps.testing");
			
			//Clicking on Before input box:
			WebDriverWait wait1 = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("updatedBeforeFilterValue")));
			//Clicking on After input box:
			driver.findElementById("updatedBeforeFilterValue").click();
			driver.context("NATIVE_APP");
			
			//On selecting previous month:
			driver.findElementById("android:id/prev").click();
			driver.findElementById("android:id/prev").click();
			
			//On selecting date:
			driver.findElementByXPath(dateUpdatedBefore).click();
			//On clicking SET option:
			driver.findElementById("android:id/button1").click();
			driver.context("WEBVIEW_com.propertyapps.testing");

			
		}
				
	}
	public static void filterByStatus() throws MalformedURLException
	{
		if(statusOption){
			WebDriverWait wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("status")));
			//Clicking on Status option:
	        tempObj1=getElement("status","","id");
			JavascriptExecutor jse11 = (JavascriptExecutor) driver;
			jse11.executeScript("arguments[0].scrollIntoView(true);", tempObj1);
			//Selecting Status option
			tempList=getRadioButton("status","ion-radio","id");
			tempList.get(inputStatus).click();
			
		}
		
	}
		
	public static void filterByPriority() throws MalformedURLException
	{
		if(priorityOption){
			WebDriverWait wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("priority")));
			//Clicking on Priority option
			tempList=getRadioButton("priority","ion-radio","id");
			tempList.get(inputPriority).click();
			
		}
		
	}
	
	public static void filterByCost() throws MalformedURLException
	{
		if(costOption){
			WebDriverWait wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("costGreaterThan")));
			//Filtering by entering cost in costGreaterThan input: 
			tempObj1=getElement("costGreaterThan","ion-input","id");
			JavascriptExecutor jse1 = (JavascriptExecutor) driver;
			jse1.executeScript("arguments[0].scrollIntoView(true);", tempObj1);
			tempObj1.sendKeys(costGreaterThanInput);
			driver.pressKeyCode(AndroidKeyCode.BACK);
			
			WebDriverWait wait1 = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("costLessThan")));
			//Filtering by entering cost in costLessThan input: 
			tempObj1=getElement("costLessThan","ion-input","id");
			tempObj1.sendKeys(costLessThanInput);
			driver.pressKeyCode(AndroidKeyCode.BACK);

		}
			}
	
	public static void filterBySelectingAssignedTo() throws MalformedURLException
	{
		if(assignedToOption){
			WebDriverWait wait1 = new WebDriverWait(driver, 40);
			wait1.until(ExpectedConditions.visibilityOfElementLocated(By.id("assignTo")));
			//Selecting Assigned option
			tempList=getRadioButton("assignTo","ion-radio","id");
			tempList.get(inputAssignTo).click();
			
		}
		
	
	}
	
	public static void onApplyingFilters() throws MalformedURLException
	{
		
		WebDriverWait wait1 = new WebDriverWait(driver, 40);
		
		wait1.until(ExpectedConditions.visibilityOfElementLocated(By.id("applyBtn")));
		tempObj1=getElement("applyBtn","","id");
		
		tempObj1.click();
		
		
				
	}
	
		
			public static void filteringAndSorting() throws MalformedURLException, MyError
		{
			try{
				 gotoServiceRequest();
				
			}
			catch(Exception e)
			{
				throw new MyError("Error:Problem in clicking on SHOWING FILTERS option "); 
			}
			
			try
			{
				selectingSortingOption();
				
			}
			catch(Exception e)
			{
				throw new MyError("Error:Problem on selecting radio button under Sorting Option"); 
			}
			
			try
			{
				 filterByCreatedDate();
				
			}
			catch(Exception e)
			{
				throw new MyError("Error:Problem on selecting date under Created Option"); 
			}
			
			try
			{
				filterByUpdatedDate(); 
			}
			catch(Exception e)
			{
				throw new MyError("Error:Problem on selecting date under Updated Option"); 
			}
			
			try
			{
				 filterByStatus() ; 
			}
			catch(Exception e)
			{
				throw new MyError("Error:Problem on selecting radio button under Status Option"); 
			}
			
			try
			{
				 filterByPriority(); 
			}
			catch(Exception e)
			{
				throw new MyError("Error:Problem on selecting radio button under Priority Option"); 
			}
			
			try
			{
				filterByCost(); 
			}
			catch(Exception e)
			{
				throw new MyError("Error:Problem on entering input under Cost Option"); 
			}
			
			try
			{
				 filterBySelectingAssignedTo();
			}
			catch(Exception e)
			{
				throw new MyError("Error:Problem on selecting radio button under AssignedTo Option"); 
			}
			
			try
			{
				 onApplyingFilters();
			}
			catch(Exception e)
			{
				throw new MyError("Error:Problem while clicking on  Apply button "); 
			}
			
		
			
		}
		
	
	
	
	

}
