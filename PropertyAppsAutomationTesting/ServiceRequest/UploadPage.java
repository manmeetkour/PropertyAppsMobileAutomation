package ServiceRequest;

import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import PropertyAppsAutomationTesting.basePage;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;

public class UploadPage extends basePage {

	public static void main(String[] args) throws MalformedURLException {
		// TODO Auto-generated method stub
		capabilities();
		WebDriverWait wait = new WebDriverWait(driver,40);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("module1")));
		
		WebElement tempObj1;
        tempObj1=getElement("module1 ","","class");
        
		
		JavascriptExecutor jse1 = (JavascriptExecutor) driver;
		jse1.executeScript("arguments[0].scrollIntoView(true);", tempObj1);
		tempObj1.click();
		
		WebDriverWait wait111 = new WebDriverWait(driver,40);
		wait111.until(ExpectedConditions.visibilityOfElementLocated(By.id("workOrder0")));
		tempObj1=getElement("workOrder0","ion-text","id");
		tempObj1.click();
		
		//Clicking on Upload Button
		WebDriverWait wait1111 = new WebDriverWait(driver,40);
		wait1111.until(ExpectedConditions.visibilityOfElementLocated(By.id("uploadBtn")));
		tempObj1=getElement("uploadBtn","","id");
		tempObj1.click();
		
		//Click on Upload Photo  Option:
		List<AndroidElement> tempList;
		tempList=returnMatchingElements("action-sheet-button","class");
		tempList.get(1).click();
		driver.pressKeyCode(AndroidKeyCode.BACK);
		
		
	
		tempList=returnMatchingElements("action-sheet-button","class");
		tempList.get(1).click();
		
		driver.context("NATIVE_APP");
		System.out.println(driver.getContext());
		 // on selecting photo from gallery
		driver.findElementById("com.android.documentsui:id/icon_thumb").click();
		
		
		
//		// on clicking photo by camera:
//		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//		driver.findElementById("com.android.camera2:id/shutter_button").click();
//		
//		driver.findElementById("com.android.camera2:id/done_button").click();
		
		driver.context("WEBVIEW_com.propertyapps.testing");
		WebDriverWait wait1 = new WebDriverWait(driver,10);
		wait1.until(ExpectedConditions.visibilityOfElementLocated(By.id("bankToServiceReqBtn")));
      

		
		

	}

}
